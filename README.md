# What does it do? 
This is a simple script to look for domains/urls that are linked to phishing/social engineering for a particular brand of interest. It uses 2 components: Virus Total and WebRisk API from Google Cloud. 
- Virus Total is used to search for the potential domains that could be linked to brand-related phishing campaigns or abuse.
- Once these domains are obtained, they are submitted to the WebRisk Submission API for takedown. 


# High level steps to get started:

## Setup 

### 1. Prepare Python environment
- This script works well with Python 3.8.9 or higher. Follow the instructions on this [page](https://cloud.google.com/python/docs/setup#linux) to install Python and Python Virtual Environment (***venv***). 
- Use ***venv*** to isolate dependencies for this project:

        cd your-project-folder
        python3 -m venv env
- Activate the virtual environment
        source env/bin/activate

<u>**Note***</u>: When you are done, you can stop using the virtual environment and go back to your global Python by deactivating the virtual environment with the following command. 

        deactivate


### 2. Install requirements.txt inside the virtual environment
    pip3 install -r requirements.txt 

### 3. Create config.py file with the following entries (at minimum)
    PARENT_VALUE="projects/<insert project id>"
    VT_API_KEY="<insert your VT api key>" 

### 4. Provide reference to .json key from Google Cloud Project in code 
This is referenced under ***submit.py*** and ***vt.py*** files as: 

    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '<reference the json file>.json'


## Files 

### 1. scamurls.txt
This is populated with URLs that are deemed suspicious for phishing scam. The URLs are updated and separated by commas. This file can be populated by running ***main.py*** or ***vt.py***. 

For an integrated URL takedown experience, run ***main.py***. The URLs updated in this file will be submitted to the WebRisk API. 

For a quick test on the URLs that are populated into this file, run ***vt.py***. 

### 2. urls.txt 
This is a sample test file that can be used either with _submit.py_ or with ***webrisk.py***. 
The ***webrisk.py*** file is using the Evaluate API from Webrisk and it is only looking up the URLs for a score within the Webrisk API database. 

## How to use?
Use ***main.py*** as the primary execution file. Once you have your VT search query finalized for your abuse/brand protection/scam use-case, enter the value into the variable: 
> SEARCH_PARAM

For example:

    SEARCH_PARAM = 'entity:url (category:phishing or engines:phishing) p:19+ url response_code:200 fs:2022-05-01+' 

Once this variable is updated with the revelant search query, execute the ***main.py*** file. 

This will trigger the following workflow: 
1. VT search API will pull out a list of all URLs that qualify the search criteria.
2. These URLs will be updated into ***scamurls.txt*** file.
3. Webrisk Submission API will extract the URLs from the file and submit them for takedown.
4. In less than 60 minutes, the submitted URLs will be blocked on 4B+ devices. 

