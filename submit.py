# Sample code available at: https://cloud.google.com/python/docs/reference/webrisk/latest/google.cloud.webrisk_v1.services.web_risk_service.WebRiskServiceClient#google_cloud_webrisk_v1_services_web_risk_service_WebRiskServiceClient_create_submission

import os
from google.cloud import webrisk_v1
import config

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'phishing-project-351301-d8a32d2a1333.json'

# using normal search/evaluate api 
def scamurl_create_submission(sample_uri):
    # Create a client
    client = webrisk_v1.WebRiskServiceClient()
    #Initialize request argument(s)
    submission = webrisk_v1.Submission()
    submission.uri = sample_uri

    request = webrisk_v1.CreateSubmissionRequest(
        parent=config.PARENT_VALUE,
        submission=submission,
    )
    # Make the request
    response = client.create_submission(request=request)
    # Handle the response
    print(response)

def scamurl_file(filename):
    with open(filename, "r") as fileobject:
        file_content = fileobject.read()
        # Split the file elements into a list
        list_uris = file_content.split(",") 
        # Remove whitespaces from the list elements
        list_uris = [x.strip("\n") for x in list_uris] 
        # Remove empty strings from the list 
        list_uris = list(filter(lambda x:x != "", list_uris)) 
   
    for i in list_uris:
        #print(i)
        scamurl_create_submission(i)


# scamurl_file("urls.txt")
# scamurl_file("scamurls.txt")