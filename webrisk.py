import os
from google.cloud import webrisk_v1
import json

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'phishing-project-351301-d8a32d2a1333.json'

# using normal search/evaluate api 
def evaluate_search_uris(sample_uri):
    # Create a client
    client = webrisk_v1.WebRiskServiceClient()

    # Initialize request argument(s)
    request = webrisk_v1.SearchUrisRequest(
        ##uri="http://poslogistikid-ems.com/IDJADikadL/POEGJAj28d/20232723-50ID41861547UID_25012021-ID-id_url.html=db6bd33b8a3c61044e1873edd4ace9e6ca",
        uri=sample_uri,
        threat_types=[webrisk_v1.ThreatType.SOCIAL_ENGINEERING, webrisk_v1.ThreatType.MALWARE, webrisk_v1.ThreatType.UNWANTED_SOFTWARE],
    )

    # Make the request
    response = client.search_uris(request=request)
    # Handle the response
    print(response) 
    # print(response.threat)

def evaluate_list(filename):
    count=0
    with open(filename, "r") as fileobject:
        file_content = fileobject.read()
        # Split the file elements into a list
        list_uris = file_content.split(",") 
        # Remove whitespaces from the list elements
        list_uris = [x.strip("\n") for x in list_uris] 
        # Remove empty strings from the list 
        list_uris = list(filter(lambda x:x != "", list_uris)) 

    for i in list_uris:
        count +=1 
        #print(count)
        print("URL " + str(count) + ": "+ i)
        evaluate_search_uris(i)
    print ("Total number of URLs: " + str(count))

evaluate_list("urls.txt")