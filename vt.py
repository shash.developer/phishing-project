import os
from webbrowser import get 
import requests 
import urllib.parse
import config

# # Variable declarations that are moved to main.py file 
# SEARCH_PARAM = 'entity:url (category:phishing or engines:phishing) p:19+ url response_code:200 fs:2022-05-01+'
# LIMIT = '10' 
# ENCODED_SEARCH = urllib.parse.quote(SEARCH_PARAM)
# SEARCH_URL = "https://www.virustotal.com/api/v3/intelligence/search?query=" + ENCODED_SEARCH + "&limit=" + LIMIT + "&descriptors_only=true"
COUNT = 0

# first cut - using FQDN. Needs to be broken down
# URL = "https://www.virustotal.com/api/v3/intelligence/search?query=entity%253Aurl%2520AND%2520engines%253Aphishing%2520AND%2520p%253A3%252B%2520AND%2520fs%253A2d%252B&limit=2&descriptors_only=false"

headers = {
    "Accept": "application/json",
    "x-apikey": config.VT_API_KEY
}

def get_scamdata(url,headers):
  response = requests.get(url, headers=headers)
  data = response.json()
  return data

def get_scamurl(scamdata,filename):
    global COUNT
    with open(filename,"a") as file_object: 
        for i in range(len(scamdata['data'])):
            if 'url' in scamdata['data'][i]['context_attributes']:
                # print (i)
                print (scamdata['data'][i]['context_attributes']['url'])
                file_object.write(scamdata['data'][i]['context_attributes']['url'] + ",\n")
                COUNT += 1
        # print (COUNT)

# Commenting the below section as it is not moved to the main.py file.

# def main():
#     DATA = get_scamdata(SEARCH_URL,headers)
#     # total_hits = DATA['meta']['total_hits']
    
#     get_scamurl(DATA,"scamurls.txt")
#     # print (COUNT)

#     while 'next' in DATA['links']:
#         next_url = DATA['links']['next']
#         if 'next_url':
#             DATA = get_scamdata(next_url,headers)
#             get_scamurl(DATA,"scamurls.txt")
#     else: 
#         print (COUNT)


# if __name__ == "__main__":
#     main()

