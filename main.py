import config 
import vt
import submit 
import urllib.parse

# Variable declarations
# UNCOMMENT THIS LINE TO TEST or add a search param under SEARCH_PARAM variable
# SEARCH_PARAM = 'entity:url (category:phishing or engines:phishing) p:19+ url response_code:200 fs:2022-05-01+'
SEARCH_PARAM = ''
LIMIT = '10' 
ENCODED_SEARCH = urllib.parse.quote(SEARCH_PARAM)
SEARCH_URL = "https://www.virustotal.com/api/v3/intelligence/search?query=" + ENCODED_SEARCH + "&limit=" + LIMIT + "&descriptors_only=true"
# COUNT = 0

headers = {
    "Accept": "application/json",
    "x-apikey": config.VT_API_KEY
}

def main():
    DATA = vt.get_scamdata(SEARCH_URL,headers)
    # total_hits = DATA['meta']['total_hits']
    
    vt.get_scamurl(DATA,"scamurls.txt")
    # print (COUNT)

    while 'next' in DATA['links']:
        next_url = DATA['links']['next']
        if 'next_url':
            DATA = vt.get_scamdata(next_url,headers)
            vt.get_scamurl(DATA,"scamurls.txt")
    else: 
        print (vt.COUNT)

    # submit the urls 
    # scamurl_file("urls.txt")
    submit.scamurl_file("scamurls.txt")


if __name__ == "__main__":
    main()

